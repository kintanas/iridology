<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Auth::routes();

//Login Admin//
Route::group(['prefix' => '/'], function () {
    Route::get('/', function () {
        return view('index');
    });
    Route::get('login', 'LoginController@index')->name('login');
    Route::get('logout', 'LoginController@logout');
    Route::post('validate-login', 'LoginController@login');
});

Route::get('/home', 'HomeController@index');
 Route::resource('modul', 'ModulsController')->middleware('verify-login');

// Pengguna //
Route::group(['prefix' => 'user', 'middleware' => 'verify-login'], function () {
    Route::get('/', 'UserController@index');
    Route::post('/form', 'UserController@create');
    Route::post('/save', 'UserController@save');
    Route::post('/delete', 'UserController@delete');
});


// Modul //
Route::group(['prefix' => 'moduls',  'middleware' => 'verify-login'], function () {
    Route::get('/', 'ModulsController@index');
    Route::get('/create', 'ModulsController@store');
    Route::post('/delete', 'ModulsController@destroy');
    Route::get('/{id}/edit','ModulsController@edit');
    Route::put('/list/{id}','ModulsController@update');
    Route::delete('/list/{id}','ModulsController@destroy');
    Route::post('/delete-gambar/{id}','ModulsController@deleteGambar');
});

// Video //
Route::group(['prefix' => 'video', 'middleware' => 'verify-login'], function () {
    Route::get('/', 'VideoController@index');
    Route::get('/add', 'VideoController@create');
    Route::post('/save', 'VideoController@store');
    Route::get('/edit/{id}','VideoController@edit');
    Route::post('/update/{id}','VideoController@update');
    Route::delete('/delete/{id}','VideoController@destroy');
   

    // Route::post('/save', 'VideoController@save');
    // Route::post('/delete', 'VideoController@delete');
});

