<!DOCTYPE html>
<html>
<!-- head -->
@include('partials._head')

<body>
  <!-- Sidenav -->

  @include('partials._sidebar')
  <!-- Main content -->
  <div class="main-content">
    <!-- Top navbar -->
    @include('partials._header')

    @yield('content')

  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="{{ asset('assets/vendor/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>    
  <script src="{{ asset('assets/vendor/js-cookie/js.cookie.js') }}"></script>
  <script src="{{ asset('assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/lavalamp/js/jquery.lavalamp.min.js') }}"></script>
  <!-- Optional JS -->
  <script src="{{ asset('assets/vendor/chart.js/dist/Chart.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/chart.js/dist/Chart.extension.js') }}"></script>
  <script src="{{ asset('assets/vendor/jvectormap-next/jquery-jvectormap.min.js') }}"></script>
  <script src="{{ asset('assets/js/vendor/jvectormap/jquery-jvectormap-world-mill.js') }}"></script>

  <script src="{{ asset('assets/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/datatables.net-select/js/dataTables.select.min.js') }}"></script>

  <!-- Docs JS -->
  <script src="{{ asset('assets/vendor/anchor-js/anchor.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/clipboard/dist/clipboard.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/holderjs/holder.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/prismjs/prism.js') }}"></script>
  <!-- Argon JS -->
  <script src="{{ asset('assets/js/argon.min-v=1.0.0.js') }}"></script>
  <!-- Demo JS - remove this in your project -->
  <script src="{{ asset('assets/js/demo.min.js') }}"></script>

  <!-- Ajax -->
  <input type='hidden' name='_token' value='{{ csrf_token() }}'>
  @yield('scripts')

  <script>
      baseURL = '{{url("/")}}';
  </script>
  @yield('style')
  @yield('script')
  <script src="{{asset('assets/corelib/core.js')}}"></script>

  <script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
  <script>
    CKEDITOR.replace( 'editor1' );
  </script>


</body>

</html>