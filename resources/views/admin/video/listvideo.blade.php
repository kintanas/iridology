@extends('partials._design')
@section('content')
<div class="container-fluid mt-4">
    <div class="row">
        <div class="col">
            <div class="card">
                <!-- Card header -->
                <div class="card-header">
					Data Video dan Animasi
						<a style="float: right;" class="btn btn-primary" onclick="location.href='{{ url('/video/add') }}'">Add</a>
                </div>
                <div class="table-responsive py-4">

					@if(Session::has('alert-success'))
		                <div class="alert alert-success">
		                    <strong>{{ \Illuminate\Support\Facades\Session::get('alert-success') }}</strong>
		                </div>
		            @endif

                    <table class="table table-flush" id="datatable-basic">
                        <thead class="thead-light">
                            <tr>
								<th style="width: 5%">#</th>
								<th style="width: 20%">Judul</th>
								<th style="width: 15%">Sumber</th>
								<th style="width: 15%">File Video</th>
								<th style="width: 30%">Keterangan</th>
								<th style="width :15%"></th>
                            </tr>
                        </thead>
                        <tbody>
							@php $no = 1; @endphp
							@foreach ($video as $item)
								<tr>
									<td>{{ $no++ }}</td>
									<td>{{ $item->judul }}</td>
									<td>{{ $item->sumber }}</td>
									<td>{{ $item->file }}</td>
									<td>{{ $item->keterangan }}</td>
									<td>
										<form action="{{ url('/video/delete', $item->id) }}" method="post">
										{{ csrf_field() }}
										{{ method_field('DELETE') }}
										<a class="btn btn-sm btn-info" href="{{ url('video/edit')}}/{{$item->id }}">Edit</a>
										<button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Delete</button>
										</form>
									</td>
								</tr>
							@endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection