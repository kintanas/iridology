@extends('partials._design')

@section('content')
<script type="text/javascript">
    var baseImgpath = "{{ asset('storage/images') }}"
</script>
<div class="container">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                	Edit Data Modul
                	<a style="float: right;" class="btn btn-primary" href="{{ route('modul.index') }}">Back</a>
            	</div>

                <div class="card-body">
                    
                   <!--  @if(@$modul->exists) <?php  $url //= route('modul.update', @$modul->id); ?>
                    @else <?php  $url //= route('modul.store'); ?>
                    @endif -->
                	<form class="form-horizontal" enctype="multipart/form-data" action="{{ route('modul.update',$modul->id)}}" method="POST" onsubmit="return getContent();" id="modulForm">
                        
                     <!--    @if (@$model->exists)
                            {{ method_field('PUT') }}
                        @else
                            {{ method_field('POST') }}
                        @endif
 -->
                        {{ csrf_field() }}
                        {{ method_field('PUT')}}

                        <!-- <div class="form-group{{ $errors->has('nomor') ? ' has-error' : '' }}">
                            <label for="bab" class="col-md-4 control-label">Bab</label>

                            <div class="col-md-6">
                                <input id="bab" type="text" class="form-control" name="bab" value="{{ @$modul->bab }}" required>

                                @if ($errors->has('bab'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('bab') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> -->

                        <div class="form-group{{ $errors->has('judul') ? ' has-error' : '' }}">
                            <label for="judul" class="col-md-4 control-label">Judul</label>

                            <div class="col-md-6">
                                <input id="judul" type="text" class="form-control" name="judul" value="{{ @$modul->judul }}" required>

                                @if ($errors->has('judul'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('judul') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('sumber') ? ' has-error' : '' }}">
                            <label for="sumber" class="col-md-4 control-label">Sumber</label>

                            <div class="col-md-6">
                                <input id="sumber" type="text" class="form-control" name="sumber" value="{{ @$modul->sumber }}" required>

                                @if ($errors->has('sumber'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sumber') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                    
                        <div class="col-md-12 row" id="dynamic-document">
                                @foreach($gambar as $key => $gmbr)
                                
                        <div class="col-md-4 gmbr-{{$key}}">
                                    
                        <input type="hidden" name="filename[]" class="new-value-{{$key}}" value="{{str_replace('public/images/','',$gmbr->nama_file)}}">
                                        <img src="{{env('APP_URL').'/storage/app'}}/{{$gmbr->nama_file}}" width="200px"> 
                                        
                                </div>
                                <div class="col-md-4 gmbr-{{$key}}">

                                        <div class="form-group">
    
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="custom-file" id="edit_file"> 
                                                        <input type="file" name="file[]" class="custom-file-input  change-{{$key}}" id="customFileLang"   onchange="chooseValue({{$key}})"lang="en">
                                                        <label class="custom-file-label label-change-{{$key}}" for="customFileLang"></label>          
                                                        </div>
            
                                                    </div>
                                                </div>
                                            </div>
                                </div>
                            <div class="col-md-4 gmbr-{{$key}}">
                                    @if($key==0)
                                        <button type="button" class="btn btn-info" id="add-document"><i class="fa fa-plus"></i></button>
                                     @else
                                        <button type="button" class="btn btn-danger btn_remove" id="{{$key}}" value="{{$gmbr->id}}">x</button>
                                    @endif
                                    
                                </div>
                                @endforeach
                                
                            </div>
                            <br>
                            <br>

                        <div class="konten-cont col-lg-12 {{ $errors->has('konten') ? ' has-error' : '' }}">
                            <div class="editor-wrap">
                                <textarea id="editor1" style="display:none" name="konten" class="text-md-left" contenteditable="true">{!! $modul->konten !!}></textarea>
                            </div>
                            @if ($errors->has('sumber'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('sumber') }}</strong>
                                </span>
                            @endif
                        </div>
                       

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group float-right">
                                    <button type="submit" class="btn btn-primary">Update
                                        <span class="btn-inner--icon"><i class="ni ni-check-bold"></i></span>
                                        <!--    @if (@$modul->exists)
                                                   Update 
                                                @else
                                                    Add 
                                                @endif -->
                                    </button>
                                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                                    <input type="hidden" id="count" value="{{$gambar->count()}}">
                                </div>
                            </div>
                        </div>

                    
                        {{-- <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary"> Update
                                  <!--   @if (@$modul->exists)
                                        Update 
                                    @else
                                        Add 
                                    @endif -->
                                </button>
                                 {{ csrf_field() }}
                            </div>
                        </div> --}}
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>

@section('script')
    <script>
        CKEDITOR.replace( 'editor1' );
    </script>
    <script>
     function addFile() {
            var i =$("#count").val();
            $('#add-document').click(function(){
                i++;
                $('#dynamic-document').append('<div class="col-md-4 gmbr-'+i+'"></div><div class="col-md-4 gmbr-'+i+'"><div class="form-group"><div class="input-group"><div class="input-group-prepend"><div class="custom-file" id="edit_file"> <input type="file" name="file[]" class="custom-file-input change-'+(i)+'"  onchange="chooseValue('+(i)+')" id="customFileLang" lang="en"><label class="custom-file-label label-change-'+(i)+'" for="customFileLang"></label></div></div></div></div></div><div class="col-md-4 gmbr-'+i+'"><button type="button" class="btn btn-danger btn_remove" id="'+i+'">x</button></div>');
            });
            $(document).on('click', '.btn_remove', function(){
                var button_id = $(this).attr("id");
                var keys=$(this).attr("value");
                var csrfParam = '_token';
                csrfToken = $('input[name='+csrfParam+']').val();
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': csrfToken
                    },
                    url: "{{url('moduls/delete-gambar/')}}/"+keys+"",
                    success: function(){
                    }
                });
                $('.gmbr-'+button_id+'').remove();
                i--;
            });
        }

        $(document).ready(function () {

            addFile();
        
        })
    
    
    </script>
    <script>
     function chooseValue(i){
            var fileName=$('.change-'+i).val()
            fileName=fileName.substring(fileName.lastIndexOf("\\")+1,fileName.length)
            $(".label-change-"+i).html(fileName)
            $(".new-value-"+i).val(fileName)

        }
    </script>
@endsection

@endsection
