@extends('partials._design')
@section('content')
<div class="container-fluid mt-4">
    <div class="row">
        <div class="col">
            <div class="card">
                <!-- Card header -->
                <div class="card-header">
					Data Moduls
						<a style="float: right;" class="btn btn-primary" onclick="location.href='{{ route('modul.create') }}'">Add</a>
                </div>
                <div class="table-responsive py-4">

					@if(Session::has('alert-success'))
		                <div class="alert alert-success">
		                    <strong>{{ \Illuminate\Support\Facades\Session::get('alert-success') }}</strong>
		                </div>
		            @endif

                    <table class="table table-flush" id="datatable-basic">
                        <thead class="thead-light">
                            <tr>
								<th>No</th>
								<th>Judul</th>
								<th>Sumber</th>
								<th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
							@php $no = 1; @endphp
							@foreach ($modul as $item)
								<tr>
									<td>{{ $no++ }}</td>
									<td>{{ $item->judul }}</td>
									<td>{{ $item->sumber }}</td>
									<td>
										<form action="{{ route('modul.destroy', $item->id) }}" method="post">
										{{ csrf_field() }}
										{{ method_field('DELETE') }}
										<a class="btn btn-sm btn-info" href="{{ route('modul.edit', $item->id) }}">Edit</a>
										<button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Delete</button>
										</form>
									</td>
								</tr>
							@endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection