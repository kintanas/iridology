@extends('partials._design')
@section('content')
<div class="container-fluid mt-4">
    <div class="row">
        <div class="col">
            <div class="card">
                <!-- Card header -->
                <div class="card-header">
                    Data User Pengguna
                        <a style="float: right; " onclick="loadModal(this)" target="/user/form" class="btn btn-icon icon-left btn-secondary " title="Create New">
                            <i class="far fa-edit"></i> Create New User</a>
                </div>
                <div class="table-responsive py-4">
                    <table class="table table-flush" id="datatable-basic">
                        <thead class="thead-light">
                            <tr>
                                <th class="text-center">#</th>
                                <th>Name</th>
                                <th>Jenis Kelamin</th>
                                <th>No Telpon</th>
                                <th>Alamat</th>
                                <th>Email</th>
                                <th>Password</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($pengguna as $i => $item)
                                <tr>
                                    <td>{{ $i+1 }}</td>
                                    <td>{{ $item->name}}</td>
                                    <td>{{ $item->jenis_kelamin}}</td>
                                    <td>{{ $item->no_telp}}</td>
                                    <td>{{ $item->alamat}}</td>
                                    <td>{{ $item->email}}</td>
                                    <td>{{ $item->password}}</td>
                                    <td>
                                        <button type="button" class="btn btn-icon icon-left btn-info" onclick="loadModal(this)" title="" target="/user/form" data="id={{$item->id}}">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                        <button type="button" class="btn btn-icon icon-left btn-danger" onclick="deleteUser({{$item->id}})">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@section('scripts')
    <script>
        function deleteUser(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("", "Do you want to remove this data ?", function () {
               ajaxTransfer("/user/delete", data, "#modal-output");
            })
        }
    </script>
@endsection

@endsection