@extends('partials._design')
@section('content')

<!-- Page content -->
<div class="container-fluid mt-4">
        <!-- Table -->
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header">
                        

                        <!-- Search form -->
                        <form class="navbar-search navbar-search-light form-inline mr-sm-3" id="navbar-search-main">
                            <div class="form-group mb-0">
                                <div class="input-group input-group-alternative input-group-merge">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-search"></i></span>
                                    </div>
                                    <input class="form-control" placeholder="Search" type="text">
                                </div>
                            </div>
                            <button type="button" class="close" data-action="search-close" data-target="#navbar-search-main" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            Data User Pengguna
                                <a style="float: right; " onclick="loadModal(this)" target="/user/form" class="btn btn-icon icon-left btn-secondary " title="Create New">
                                    <i class="far fa-edit"></i> Create New User</a>
                        </form>
                    </div>

                    
                    <div class="table-responsive py-4">
                        @if(Session::has('alert-success'))
                            <div class="alert alert-success">
                                <strong>{{ \Illuminate\Support\Facades\Session::get('alert-success') }}</strong>
                            </div>
                        @endif
                        <table class="table table-flush" id="datatable-basic">
                                <thead>
                                    <th class="text-center">#</th>
                                    <th>Name</th>
                                    <th>Jenis Kelamin</th>
                                    <th>No Telpon</th>
                                    <th>Alamat</th>
                                    <th>Email</th>
                                    <th>Password</th>
                                    <th>Action</th>
                                </thead>
                                <tbody>
                                    @foreach($pengguna as $i => $item)
                                        <tr>
                                            <td>{{ $i+1 }}</td>
                                            <td>{{ $item->name}}</td>
                                            <td>{{ $item->jenis_kelamin}}</td>
                                            <td>{{ $item->no_telp}}</td>
                                            <td>{{ $item->alamat}}</td>
                                            <td>{{ $item->email}}</td>
                                            <td>{{ $item->password}}</td>
                                            <td class="align-middle">  </td>
                                            <td>
                                                <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                    <button type="button" class="btn btn-icon icon-left btn-info" onclick="loadModal(this)" title="" target="/user/form" data="id={{$item->id}}">
                                                        <i class="fas fa-edit"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-icon icon-left btn-danger" onclick="deleteUser({{$item->id}})">
                                                        <i class="fas fa-trash"></i>
                                                    </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@section('scripts')
    <script>
        function deleteUser(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("", "Do you want to remove this data ?", function () {
               ajaxTransfer("/user/delete", data, "#modal-output");
            })
        }
    </script>
@endsection
@endsection