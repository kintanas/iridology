<div id="result-form-konten"></div>
<div class="card bg-secondary border-0 mb-0">
    <div class="card-body px-lg-5 py-lg-5">
        <div class="text-center text-muted mb-4">
            <h1 class="modal-title" id="title">Add New User</h1>
        </div>
        <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
            <div class="form-group mb-3">
                <div class="input-group input-group-merge input-group-alternative">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-circle-08"></i></span>
                    </div>
                <input type="text" name="name" class="form-control" placeholder="Name" value="{{$data->name}}" required="">
                </div>
            </div>

            <!-- Checkboxes and radios -->
            <div class="form-group">
                <label>Jenis Kelamin</label>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="radio radio-info" style="margin:6px 0">
                            <input type="radio" name="jenis_kelamin" <?php if($data->jenis_kelamin =='Laki-Laki') {echo "checked";}?> value="Laki-Laki">
                            <label for="casing1">Laki-Laki</label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="radio radio-info" style="margin:6px 0">
                            <input type="radio" name="jenis_kelamin" <?php if($data->jenis_kelamin =='Perempuan') {echo "checked";}?> value="Perempuan">
                            <label for="casing2">Perempuan</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group mb-3">
                <div class="input-group input-group-merge input-group-alternative">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-mobile-button"></i></span>
                    </div>
                <input type="number" min='0' name="no_telp" class="form-control" placeholder="08xx-xxxx-xxxx" value="{{$data->no_telp}}" required="">
                </div>
            </div>

            <div class="form-group mb-3">
                <div class="input-group input-group-merge input-group-alternative">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-shop"></i></span>
                    </div>
                <input type="text" name="alamat" class="form-control" placeholder="Alamat" value="{{$data->alamat}}" required="">
                </div>
            </div>

            <div class="form-group mb-3">
                <div class="input-group input-group-merge input-group-alternative">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                    </div>
                    <input type="email" name="email" class="form-control" placeholder="Email" value="{{$data->email}}" required="">
                </div>
            </div>
            <div class="form-group">
                 @if($data->password == null)
                <div>    
                </div>
                  @else
                  <div class="input-group input-group-merge input-group-alternative">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                        </div>
                        <input class="form-control" readonly placeholder="Password" type="password" name="password" value="{{$data->password}}">
                    </div>
                  @endif

            </div>
            <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                    <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
                      &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
             </div>
             <input type='hidden' name='id' value='{{ $data->id }}'>
             <input type='hidden' name='_token' value='{{ csrf_token() }}'>
        </form>
    </div>
</div>

<script>
        $(document).ready(function () {
            $('#form-konten').submit(function () {
                var data = getFormData('form-konten');
                ajaxTransfer('/user/save', data, '#result-form-konten');
            })
        })
    </script>