 $(function(){
 	let unescape = _.unescape(document.getElementById("konten-area").innerHTML);
 	document.getElementById("konten-area").innerHTML = unescape;

 	$('[contenteditable]').on('paste', function(e) {
	    e.preventDefault();
	    var text = '';
	    if (e.clipboardData || e.originalEvent.clipboardData) {
	      text = (e.originalEvent || e).clipboardData.getData('text/plain');
	    } else if (window.clipboardData) {
	      text = window.clipboardData.getData('Text');
	    }
	    if (document.queryCommandSupported('insertText')) {
	      document.execCommand('insertText', false, text);
	    } else {
	      document.execCommand('paste', false, text);
	    }
	});
 	$("#gambar-btn").click(function(){
 		let gambarForms = document.getElementsByName("uploadgambar[]");
 		let range = window.getSelection().getRangeAt(0);
 		console.log(range.startContainer.parentNode);
 		if (range.startContainer.parentNode.id == "konten-area" || range.startContainer.attributes.id.value == "konten-area") {
	 		if (gambarForms[gambarForms.length - 1].value != '') {
			  	let createdGambar = $("<input>").attr({
	  				hidden: 'true',
	  				class: 'uploadgambar',
	  				type: 'file',
	  				name: 'uploadgambar[]',
	  			})
	  			$(".uploadgambar:last").after(createdGambar);
			}
	 		openDialog(gambarForms);
 		}
 	})

 	$(document).on("change", ".uploadgambar", function(){
 		let createdGambarHide = $("<input>").attr({
 			hidden: 'true',
 			type: 'text',
 			value: Date.now(),
 			name: 'namaGambar[]',
 			class: 'hiddenGambarInp',
 		})
 		$("#modulForm").append(createdGambarHide);
 		let gambarForms = document.getElementsByName("uploadgambar[]");
 		insertImage(gambarForms[gambarForms.length - 1].value.split(".").pop().toLowerCase());
 		displayImage(this);
 	})

 	let targetNode = document.getElementById("konten-area");

 	let config = { childList: true };

 	let callback = function(mutationList){
 		for(let mutation of mutationList){
 			if(mutation.removedNodes.length){
 				 var elem = mutation.removedNodes;
 				 // console.log(mutation);
			     if(elem[0].className == 'konten-img'){
			     	$index = elem[0].id;

			     	// var prevElement = $(mutation.previousSibling.previousElementSibling);
			     	// var removedIndex = (prevElement.index());
			     	if($index == 0){
			     		$(".uploadgambar:eq(" + $index + ")").val("");
			     		console.log($(".uploadgambar:eq(" + $index + ")").val());
			     	}
			     	else{
			     		$(".uploadgambar:eq(" + $index + ")").remove();
			     	}
			     	$(".hiddenGambarInp:eq(" + $index + ")").remove();

			     	// console.log(elem[0].index());
			     	// console.log(elem[0].id);
			     	// console.log(mutation.previousSibling);
			     }
 			}
 		}
 	}
 	let observer = new MutationObserver(callback);
	observer.observe(targetNode, config); 		

 })

function createHiddenImg(){
	// let image = $("<img>").attr({
	// 	src: $(".hiddenGambarInp:last").val(),
	// 	hidden: "true",
	// })
	// $(".konten-img:last").after(image);

	let sel, range, html;
	let filename = $(".hiddenGambarInp:last").val();

	sel = window.getSelection();
	range = sel.getRangeAt(0);
	range.deleteContents();

	let nnode = document.createElement("img");
	nnode.setAttribute("src", filename);
	nnode.setAttribute("hidden", "true");
	nnode.setAttribute("id", $("#konten-area img:last").index('.konten-img'));
	nnode.setAttribute("class", "hiddenimg");	

	// let text = document.createTextNode(filename);
	// nnode.appendChild(text)
	
	range.insertNode(nnode);
	range.setStartAfter(nnode);

	sel.removeAllRanges();
	sel.addRange(range);
}
function displayImage(input){
	 if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.konten-img:last').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

 function getContent(){
 	changeImgSrc();
 	$('#konten-area').html().replace(/<div>/gi,'<br>').replace(/<\/div>/gi,'');
 	// let text = changebr(document.getElementById("konten-area").innerHTML);
    document.getElementById("konten-textarea").value = document.getElementById("konten-area").innerHTML;
 }

 function changebr(text){
 	return (text.replace("<br>", "\n"));

 }
 function changeImgSrc(img){
 	$("#konten-area img").each(function(){
 		let src = $(this).attr("alt");
 		$(this).attr("src",  baseImgpath + "/" + src);
 	})
 }

 function openDialog(gambarForms) {
  gambarForms[gambarForms.length - 1].click();
}

function insertImage(extension){
	
	let sel, range, html;
	let doc = document.getElementById("konten-area");

	sel = window.getSelection();
	range = sel.getRangeAt(0);
	range.deleteContents();

	let nnode = document.createElement("img");
	nnode.setAttribute("class", "konten-img");
	nnode.setAttribute("width", "300");
	nnode.setAttribute("height", "300");
	nnode.setAttribute("alt", $('.hiddenGambarInp:last').val() + "." + extension);
	nnode.setAttribute("id", $("#konten-area img:last").index('.konten-img') + 1);

	// let text = document.createTextNode(filename);
	// nnode.appendChild(text)
	
	range.insertNode(nnode);
	range.setStartAfter(nnode);

	sel.removeAllRanges();
	sel.addRange(range);
}

function insertTextAtCursor(text) { 
    sel = window.getSelection();
    range = sel.getRangeAt(0); 
    var sel, range, html; 
    range.deleteContents(); 
    var textNode = document.createTextNode(text);
    range.insertNode(textNode);
    range.setStartAfter(textNode);
    sel.removeAllRanges();
    sel.addRange(range);        
}



function keyHandle(evt) {
    key = evt.keyCode;
    switch(key) {
        case 9: //Tab
            insertTextAtCursor('\t');
            evt.preventDefault();
            break
        case 13: //Enter
            insertTextAtCursor('\n');
            evt.preventDefault();
            break
    }
}