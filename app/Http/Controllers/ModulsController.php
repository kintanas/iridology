<?php

namespace App\Http\Controllers;

use App\Modul;
use App\Gambar;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Session;

class ModulsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modul = Modul::all();
        return view('admin.modul.list', ['modul' => $modul]);

    }

    // *
    //  * Show the form for creating a new resource.
    //  *
    //  * @return \Illuminate\Http\Response
     
    public function create()
    {
        $modul= modul::all();
        return view('admin.modul.form');

    }

    // *
    //  * Store a newly created resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
     
    public function store(Request $request)
    {
        // //$input = $request->file('file');
        // $file = $request->file('file'); 
        // // $extension = $file->getClientOriginalExtension(); 

        // $input = $request->all();
        // $input['file'] = time().'.'.$extension;
        // $request->file->move(public_path('upload'), $input['file']);

        // Modul::create($input);

        // return redirect()->route('modul.index')->with('alert-success','Berhasil Menambahkan Data!');
        $documentFiles=$request->file('file');
        $modul = new Modul;
        $modul->judul = $request->judul;
        $modul->sumber= $request->sumber;
        $modul->konten = $request->konten;
        $modul->save(); 

    
        
        $allowedfileExtension=['bmp','jpg','png','jpeg'];
        if ($documentFiles) {
            foreach ($documentFiles as $key=>$file) {
                $extension = strtolower($file->getClientOriginalExtension());
                $rand = str_random(5);
                $check=in_array($extension,$allowedfileExtension);
                if ($check) {
                    $gambar = new Gambar();
                    $gambar->id_modul= $modul->id;
                    $filename = $rand.$file->getClientOriginalName();
                    $gambar->nama_file = $file->storeAs('public/images', $filename);
                    $gambar->save(); 
                }
            }
        }
        
        return redirect('/moduls');

    }

    public function edit($id)
    {
        $modul = Modul::find($id);
        $gambar = Gambar::query()->where("id_modul", $id)->get();
        $modul->konten = str_ireplace("\n", "<br>", $modul->konten);                           
        return view('admin.modul.edit', ['modul' => $modul, 'gambar' => $gambar]);

    }

    public function update(Request $request, $id)
    {
        $request->konten = trim($request->konten, "");
        $request->validate([
            'judul' => 'required',
            'sumber' => 'required',
            'konten' => 'required',
        ]);
        $modul = Modul::find($id);

        
        $modul->judul = $request->judul;
        $modul->sumber= $request->sumber;
        $modul->konten = $request->konten;
        $modul->save();

        $documentFiles=$request->file('file');
        $fileLama=$request->filename;
        $checkGambarLama=$this->checkGambar($fileLama,$id);

    
        $allowedfileExtension=['bmp','jpg','png','jpeg'];
        if ($documentFiles) {
            foreach ($documentFiles as $key=>$file) {
                $rand = str_random(5);
                $extension = strtolower($file->getClientOriginalExtension());
                $check=in_array($extension,$allowedfileExtension);
                $checkLamaBaru=$this->checkGambarLamaBaru($checkGambarLama,$file->getClientOriginalName());
                if($check){
                    if($checkLamaBaru!=false){
                        $value1=explode('-',$checkLamaBaru);
                        $checkGambar=Gambar::find($value1[0]);
                        $filename = $rand.$file->getClientOriginalName();
                        $checkGambar->nama_file = $file->storeAs('public/images', $filename);
                        $checkGambar->save(); 
                    }else{
                        $gambar = new Gambar();
                        $gambar->id_modul= $id;
                        $filename = $rand.$file->getClientOriginalName();
                        $gambar->nama_file = $file->storeAs('public/images', $filename);
                        $gambar->save(); 
                    }
                }
               
            
        
            }
        }
        return redirect('/moduls')->with('alert-success','Berhasil Memperbarui!'); 
    }

    private function checkGambar($gambar,$id)
    {
        $currentData=[];
        $gambarLama=Gambar::where("id_modul", $id)->get();
        foreach($gambarLama as $key1 =>$item1){
            $namaFile=str_replace('public/images/','',$item1->nama_file);
            if($gambar[$key1]!=$namaFile){
                $currentData[]=[
                    'id'=>$item1->id,
                    'gambar_baru'=>$gambar[$key1]
                ];
            }
        }

        return $currentData;

    }

    private function checkGambarLamaBaru($gambar,$fileUpload)
    {
        foreach($gambar as $key =>$item)
        {
            if($gambar[$key]['gambar_baru']==$fileUpload){
                return $gambar[$key]['id'].'-'.$gambar[$key]['gambar_baru'];
            }else{
                return false;
            }
        }

    }
      
    public function destroy($id)
    {
         $modul = modul::find($id);
         $gambar = gambar::find($id);
         $modul -> delete();
        return redirect('/moduls')->with('alert-success','Berhasil Menghapus Data!'); 
    }

    public function get()
    {
        $modul = new Modul;
        $modul->results= Modul::all();

        
        return response()
            ->json($modul->result, 200);
    }


    public function get_judul(){
        // $modul = new Modul;
        // $modul->request = Modul::all();
        $modul = Modul::all();
        $data = Gambar::join('moduls','gambar.id_modul','=','moduls.id')->get();
        return response()->json($modul,200);

        // $judul = (object)["results" => []];
        // foreach ($modul->results as $value) {
        //     $data = (object)array("id"=>$value->id, "judul"=>$value->judul);
        //     array_push($judul->results, $data);
        // }
        // return response()->json($judul, 200);
    }
    public function get_id($id){
        // $modul = new modul;
        // $modul->result = modul::find($id);
        $data = Modul::with('gambar')->find($id);
        return response()->json($data, 200);
    }

    public function deleteGambar($id)
    {
        Gambar::find($id)->delete();
    }
}
