<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\User;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function login (Request $Request)
    {
    	if ($request->isMethod('post')) {
    		$data = $request->input();
    		if (Auth::attempt(['email' => $data['email'], 'password' => $data['password'], 'admin' => $data['admin']])) {
    			//return redirect('admin/dashboard');
    		} else {
    			return redirect ('/admin')->with('flash_message_error', 'Invalid email or password');
    		}
    	}
    	//return view('admin.admin_login');
    }

    public function dashboard()
    {
    	//return view('admin.dashboard');
    }

    public function chkPassword (Request $request)
    {
    	$data = $request->all();
    	$current_password = $data['current_pwd'];
    	$checkPassword = User::where(['password' == 'password'])->first();
    	if (Hash::check($current_password, $checkPassword->password)) {
    		echo "true";
    	} else {
    		echo "false";
    	}
    }

	public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/logout');
    }
}
