<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Pengguna;
use Validator;

class UserController extends Controller
{
	// public function indexuser()
	// {
	// 	return view('admin.user.listuser');
	// }

	public function index(){
		$data =
		[
			'pengguna'=>Pengguna::all(),
		];

        return view('admin.user.listuser',$data);
	}

	public function create(Request $request)
	{
		$id = $request->input('id');
		if ($id) {
			$data = Pengguna::find($id);
		} else {
			$data = new Pengguna();
		}
		$params = [
			'data' => $data,
		];
		return view ('admin.user.form', $params);
	}

	public function save(Request $request)
	{
		$id = intval($request->input('id',0));

		if ($id) {
			$data = Pengguna::find($id);
		} else {
			$data = new Pengguna();
		}
		$data->name = $request->name;
		$data->jenis_kelamin = $request->jenis_kelamin;
		$data->no_telp = $request->no_telp;
		$data->alamat = $request->alamat;
		$data->email = $request->email;
		$data->password = $request->email;

		if ($id) {
			$data->password = $request->password;
		} else {
			$data->password = str_random(8);
		}
	
		try{
			$data->save();
            return "
            <div class='alert alert-success'>Data Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
			// return "<div class='alert alert-danger'>Add User Failed! Data not saved!</div>";
			echo $ex;
        }
	}


    public function delete(Request $request)
    {
		$id = intval($request->input('id',0));
        try{
            Pengguna::find($id)->delete();
            return "
            <div class='alert alert-success'>User Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        }catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! User not removed!</div>";
        }

	}
	
	public function login(Request $request)
    {
		$email = $request->input('email');
		$password = $request->password;
		$check = Pengguna::where([
			'email' => $email,
			'password' => $password,
		]) -> first();
		if ($check) {
			return response()
            ->json($check, 200);
		} else {
			return response()
            ->json([], 404);
		}
        
    }

}
