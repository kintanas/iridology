<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Session;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function index(Request $request)
    {
        if ($request->session()->exists('activeUser')) {
            return redirect('/');
        }
        return view('login');
    }
 
    public function login(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');
 
        $activeUser = User::where(['email'=>$email])->first();
        // dd($activeUser->password);

        if(is_null($activeUser->password))
        {
            return redirect('/login');
        }
        else
        {
            if(Hash::check($password, $activeUser->password))
            {
                $request->session()->put('activeUser',$activeUser);
                return redirect('/home');
                //return "login berhasil";
            }
            else
            {
                return "password salah";
            }
        }
 
    }
 
    public function logout(Request $request)
    {
        Session::flush();
        return redirect('/login');
    }
}
