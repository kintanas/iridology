<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pengguna;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
 
        $activeUser = Pengguna::where('email',$email)->first();
        if ($activeUser!=null) {
            if ($activeUser->password === $password) {
                $activeUser->save();
 
                $sendingParams = [
 
                    'is_success' => true,
                    'status' => 200,
                    'message' => 'success',
                    'data' => $activeUser
                ];
 
                return response()-> json($sendingParams);
            }
        }
 
        if(is_null($activeUser)){
           $sendingParams = [
               'is_success' => false,
               'status' => 404,
               'message' => 'Email tidak terdaftar',
            ];
            return response()-> json($sendingParams);
        }
 
       if ($activeUser -> password != $password) {
            $sendingParams = [
                'is_success' => false,
                'status' => 401,
                'message' => 'password salah',
            ];
            return response()-> json($sendingParams);
        }
    }
}
