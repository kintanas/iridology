<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\Pengguna as Authenticatable;

class Pengguna extends Model
{
    protected $table = 'pengguna'; 
    protected $fillable = [
        'name', 'jenis_kelamin','no_telp','alamat','email', 'password',
    ];

    public $timestamps = false;

    protected $hidden = [
        'password', 'remember_token',
    ];
}
