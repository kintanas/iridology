<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gambar extends Model
{
    protected $table = 'gambar';
    protected $fillable = ['nama_file'];
    public $timestamps = true;

    // public function modul()
    // {
    //     return $this->belongsTo('App\Modul', 'id_modul', 'id');
    // }

    public function modul()
    {
        return $this->hasOne(Modul::class);
    }

}
