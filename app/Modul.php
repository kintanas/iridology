<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Support\Facades\DB;
// use Illuminate\Database\Query\Builder;
// use Illuminate\Http\Request;
// use App\Gambar;

class Modul extends Model
{
    protected $table = 'moduls';
    protected $fillable = ['bab','judul','sumber', 'konten'];
    public $timestamps = true;

    public function gambar()
    {
        return $this->hasMany('App\Gambar', 'id_modul', 'id');
    }

}
